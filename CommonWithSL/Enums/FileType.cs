﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonWithSL.Enums
{
    public static class FileType
    {
        public static readonly string PCMain = "PC Main";
        public static readonly string PCMainRU = "PC Main RU";
    }
}
