﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonWithSL.Enums
{
    public enum GalleryItemType
    {
        MediaObject = 0,
        EPGChannelProgramObject = 1
    }
}
