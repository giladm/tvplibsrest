﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonWithSL.Enums
{
   public static class MediaType
    {
       public static readonly string Movie = "Movie";
       public static readonly string Series = "Series";
       public static readonly string Episode = "Episode";
       public static readonly string Live = "Live";
    }
}
