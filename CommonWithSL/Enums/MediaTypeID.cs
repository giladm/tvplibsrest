﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonWithSL.Enums
{
    public enum MediaTypeID
    {
        Movie = 351,
        Series = 353,
        Person = 336,
        Package = 354,
        Preapid = 338,
        Episode = 352,
        Linear = 341,
        Sports = 342,
        Music = 343,
        Category = 355,
        Live = 357
    }
}
