﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonWithSL.Interfaces
{
    public interface IItemTemplate
    {
        string TemplateName { get; set; }
    }
}
