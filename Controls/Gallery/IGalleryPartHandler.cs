﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tvinci.Web.Controls.Gallery
{
    public interface IGalleryPartHandler
    {
        string Identifier { get; }
    }   
}
