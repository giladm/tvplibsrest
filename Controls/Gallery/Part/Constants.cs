﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tvinci.Web.Controls.Gallery.Part
{
    public enum ePagingMethod
    {
        Default,
        FromLoader
    }

    public enum eTagType
    {
        Begin,
        End
    }
}
