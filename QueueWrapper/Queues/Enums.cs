﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QueueWrapper.Queues
{
    public enum QueueAction
    {
        Publish,
        Subscribe,
        Ack
    }
    
}
