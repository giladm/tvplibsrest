// Copyright 2004, Microsoft Corporation
// Sample Code - Use restricted to terms of use defined in the accompanying license agreement (EULA.doc)

//--------------------------------------------------------------
// Autogenerated by XSDObjectGen version 1.4.4.1
// Schema file: MediaData.xsd
// Creation Date: 23/06/2013 10:56:05
//--------------------------------------------------------------

using System;
using System.Xml.Serialization;
using System.Collections;
using System.Xml.Schema;
using System.ComponentModel;

namespace TVPPro.Configuration.Media
{

    public struct Declarations
    {
        public const string SchemaVersion = "";
    }


    [Serializable]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public partial class RedirectCollection : ArrayList
    {
        public Redirect Add(Redirect obj)
        {
            base.Add(obj);
            return obj;
        }

        public Redirect Add()
        {
            return Add(new Redirect());
        }

        public void Insert(int index, Redirect obj)
        {
            base.Insert(index, obj);
        }

        public void Remove(Redirect obj)
        {
            base.Remove(obj);
        }

        new public Redirect this[int index]
        {
            get { return (Redirect)base[index]; }
            set { base[index] = value; }
        }
    }

    [Serializable]
    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public partial class MapCollection : ArrayList
    {
        public Map Add(Map obj)
        {
            base.Add(obj);
            return obj;
        }

        public Map Add()
        {
            return Add(new Map());
        }

        public void Insert(int index, Map obj)
        {
            base.Insert(index, obj);
        }

        public void Remove(Map obj)
        {
            base.Remove(obj);
        }

        new public Map this[int index]
        {
            get { return (Map)base[index]; }
            set { base[index] = value; }
        }
    }



    [XmlRoot(ElementName = "MediaData", IsNullable = false), Serializable]
    public partial class MediaData
    {

        [XmlElement(Type = typeof(TVM), ElementName = "TVM", IsNullable = false, Form = XmlSchemaForm.Qualified)]
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public TVM __TVM;

        [XmlIgnore]
        public TVM TVM
        {
            get
            {
                if (__TVM == null) __TVM = new TVM();
                return __TVM;
            }
            set { __TVM = value; }
        }

        public MediaData()
        {
        }

        public void MakeSchemaCompliant()
        {
            TVM.MakeSchemaCompliant();
        }
    }


    [XmlType(TypeName = "TVM"), Serializable]
    public partial class TVM
    {

        [XmlElement(Type = typeof(SearchValues), ElementName = "SearchValues", IsNullable = false, Form = XmlSchemaForm.Qualified)]
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public SearchValues __SearchValues;

        [XmlIgnore]
        public SearchValues SearchValues
        {
            get
            {
                if (__SearchValues == null) __SearchValues = new SearchValues();
                return __SearchValues;
            }
            set { __SearchValues = value; }
        }

        [XmlElement(Type = typeof(MediaInfoStruct), ElementName = "MediaInfoStruct", IsNullable = false, Form = XmlSchemaForm.Qualified)]
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public MediaInfoStruct __MediaInfoStruct;

        [XmlIgnore]
        public MediaInfoStruct MediaInfoStruct
        {
            get
            {
                if (__MediaInfoStruct == null) __MediaInfoStruct = new MediaInfoStruct();
                return __MediaInfoStruct;
            }
            set { __MediaInfoStruct = value; }
        }

        [XmlElement(Type = typeof(GalleryMediaInfoStruct), ElementName = "GalleryMediaInfoStruct", IsNullable = false, Form = XmlSchemaForm.Qualified)]
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public GalleryMediaInfoStruct __GalleryMediaInfoStruct;

        [XmlIgnore]
        public GalleryMediaInfoStruct GalleryMediaInfoStruct
        {
            get
            {
                if (__GalleryMediaInfoStruct == null) __GalleryMediaInfoStruct = new GalleryMediaInfoStruct();
                return __GalleryMediaInfoStruct;
            }
            set { __GalleryMediaInfoStruct = value; }
        }

        [XmlElement(Type = typeof(FlashMediaInfoStruct), ElementName = "FlashMediaInfoStruct", IsNullable = false, Form = XmlSchemaForm.Qualified)]
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public FlashMediaInfoStruct __FlashMediaInfoStruct;

        [XmlIgnore]
        public FlashMediaInfoStruct FlashMediaInfoStruct
        {
            get
            {
                if (__FlashMediaInfoStruct == null) __FlashMediaInfoStruct = new FlashMediaInfoStruct();
                return __FlashMediaInfoStruct;
            }
            set { __FlashMediaInfoStruct = value; }
        }

        [XmlElement(Type = typeof(MediaListTags), ElementName = "MediaListTags", IsNullable = false, Form = XmlSchemaForm.Qualified)]
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public MediaListTags __MediaListTags;

        [XmlIgnore]
        public MediaListTags MediaListTags
        {
            get
            {
                if (__MediaListTags == null) __MediaListTags = new MediaListTags();
                return __MediaListTags;
            }
            set { __MediaListTags = value; }
        }

        [XmlElement(Type = typeof(TagsGallery), ElementName = "TagsGallery", IsNullable = false, Form = XmlSchemaForm.Qualified)]
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public TagsGallery __TagsGallery;

        [XmlIgnore]
        public TagsGallery TagsGallery
        {
            get
            {
                if (__TagsGallery == null) __TagsGallery = new TagsGallery();
                return __TagsGallery;
            }
            set { __TagsGallery = value; }
        }

        [XmlElement(Type = typeof(AutoCompleteValues), ElementName = "AutoCompleteValues", IsNullable = false, Form = XmlSchemaForm.Qualified)]
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public AutoCompleteValues __AutoCompleteValues;

        [XmlIgnore]
        public AutoCompleteValues AutoCompleteValues
        {
            get
            {
                if (__AutoCompleteValues == null) __AutoCompleteValues = new AutoCompleteValues();
                return __AutoCompleteValues;
            }
            set { __AutoCompleteValues = value; }
        }

        [XmlElement(Type = typeof(AdvertisingValues), ElementName = "AdvertisingValues", IsNullable = false, Form = XmlSchemaForm.Qualified)]
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public AdvertisingValues __AdvertisingValues;

        [XmlIgnore]
        public AdvertisingValues AdvertisingValues
        {
            get
            {
                if (__AdvertisingValues == null) __AdvertisingValues = new AdvertisingValues();
                return __AdvertisingValues;
            }
            set { __AdvertisingValues = value; }
        }

        public TVM()
        {
        }

        public void MakeSchemaCompliant()
        {
            SearchValues.MakeSchemaCompliant();
            MediaInfoStruct.MakeSchemaCompliant();
            GalleryMediaInfoStruct.MakeSchemaCompliant();
            FlashMediaInfoStruct.MakeSchemaCompliant();
            MediaListTags.MakeSchemaCompliant();
            TagsGallery.MakeSchemaCompliant();
            AutoCompleteValues.MakeSchemaCompliant();
            AdvertisingValues.MakeSchemaCompliant();
        }
    }


    [XmlType(TypeName = "SearchValues"), Serializable]
    public partial class SearchValues
    {

        [XmlElement(ElementName = "Tags", IsNullable = false, Form = XmlSchemaForm.Qualified, DataType = "string")]
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public string __Tags;

        [XmlIgnore]
        public string Tags
        {
            get { return __Tags; }
            set { __Tags = value; }
        }

        [XmlElement(ElementName = "Metadata", IsNullable = false, Form = XmlSchemaForm.Qualified, DataType = "string")]
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public string __Metadata;

        [XmlIgnore]
        public string Metadata
        {
            get { return __Metadata; }
            set { __Metadata = value; }
        }

        [XmlElement(Type = typeof(Redirect), ElementName = "Redirect", IsNullable = false, Form = XmlSchemaForm.Qualified)]
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public RedirectCollection __RedirectCollection;

        [XmlIgnore]
        public RedirectCollection RedirectCollection
        {
            get
            {
                if (__RedirectCollection == null) __RedirectCollection = new RedirectCollection();
                return __RedirectCollection;
            }
            set { __RedirectCollection = value; }
        }

        public SearchValues()
        {
            Tags = string.Empty;
            Metadata = string.Empty;
        }

        public void MakeSchemaCompliant()
        {
        }
    }


    [XmlType(TypeName = "Redirect"), Serializable]
    public partial class Redirect
    {

        [XmlElement(Type = typeof(Mapping), ElementName = "Mapping", IsNullable = false, Form = XmlSchemaForm.Qualified)]
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public Mapping __Mapping;

        [XmlIgnore]
        public Mapping Mapping
        {
            get
            {
                if (__Mapping == null) __Mapping = new Mapping();
                return __Mapping;
            }
            set { __Mapping = value; }
        }

        public Redirect()
        {
        }

        public void MakeSchemaCompliant()
        {
            Mapping.MakeSchemaCompliant();
        }
    }


    [XmlType(TypeName = "Mapping"), Serializable]
    public partial class Mapping
    {
        [System.Runtime.InteropServices.DispIdAttribute(-4)]
        public IEnumerator GetEnumerator()
        {
            return MapCollection.GetEnumerator();
        }

        public Map Add(Map obj)
        {
            return MapCollection.Add(obj);
        }

        [XmlIgnore]
        public Map this[int index]
        {
            get { return (Map)MapCollection[index]; }
        }

        [XmlIgnore]
        public int Count
        {
            get { return MapCollection.Count; }
        }

        public void Clear()
        {
            MapCollection.Clear();
        }

        public Map Remove(int index)
        {
            Map obj = MapCollection[index];
            MapCollection.Remove(obj);
            return obj;
        }

        public void Remove(object obj)
        {
            MapCollection.Remove(obj);
        }

        [XmlElement(Type = typeof(Map), ElementName = "Map", IsNullable = false, Form = XmlSchemaForm.Qualified)]
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public MapCollection __MapCollection;

        [XmlIgnore]
        public MapCollection MapCollection
        {
            get
            {
                if (__MapCollection == null) __MapCollection = new MapCollection();
                return __MapCollection;
            }
            set { __MapCollection = value; }
        }

        public Mapping()
        {
        }

        public void MakeSchemaCompliant()
        {
        }
    }


    [XmlType(TypeName = "Map"), Serializable]
    public partial class Map
    {

        [XmlElement(ElementName = "Tag", IsNullable = false, Form = XmlSchemaForm.Qualified, DataType = "string")]
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public string __Tag;

        [XmlIgnore]
        public string Tag
        {
            get { return __Tag; }
            set { __Tag = value; }
        }

        [XmlElement(ElementName = "MediaTypeID", IsNullable = false, Form = XmlSchemaForm.Qualified, DataType = "string")]
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public string __MediaTypeID;

        [XmlIgnore]
        public string MediaTypeID
        {
            get { return __MediaTypeID; }
            set { __MediaTypeID = value; }
        }

        public Map()
        {
            Tag = string.Empty;
            MediaTypeID = string.Empty;
        }

        public void MakeSchemaCompliant()
        {
        }
    }


    [XmlType(TypeName = "MediaInfoStruct"), Serializable]
    public partial class MediaInfoStruct
    {

        [XmlElement(ElementName = "Tags", IsNullable = false, Form = XmlSchemaForm.Qualified, DataType = "string")]
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public string __Tags;

        [XmlIgnore]
        public string Tags
        {
            get { return __Tags; }
            set { __Tags = value; }
        }

        [XmlElement(ElementName = "Metadata", IsNullable = false, Form = XmlSchemaForm.Qualified, DataType = "string")]
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public string __Metadata;

        [XmlIgnore]
        public string Metadata
        {
            get { return __Metadata; }
            set { __Metadata = value; }
        }

        public MediaInfoStruct()
        {
            Tags = string.Empty;
            Metadata = string.Empty;
        }

        public void MakeSchemaCompliant()
        {
        }
    }


    [XmlType(TypeName = "GalleryMediaInfoStruct"), Serializable]
    public partial class GalleryMediaInfoStruct
    {

        [XmlElement(ElementName = "Tags", IsNullable = false, Form = XmlSchemaForm.Qualified, DataType = "string")]
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public string __Tags;

        [XmlIgnore]
        public string Tags
        {
            get { return __Tags; }
            set { __Tags = value; }
        }

        [XmlElement(ElementName = "Metadata", IsNullable = false, Form = XmlSchemaForm.Qualified, DataType = "string")]
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public string __Metadata;

        [XmlIgnore]
        public string Metadata
        {
            get { return __Metadata; }
            set { __Metadata = value; }
        }

        public GalleryMediaInfoStruct()
        {
            Tags = string.Empty;
            Metadata = string.Empty;
        }

        public void MakeSchemaCompliant()
        {
        }
    }


    [XmlType(TypeName = "FlashMediaInfoStruct"), Serializable]
    public partial class FlashMediaInfoStruct
    {

        [XmlElement(ElementName = "Tags", IsNullable = false, Form = XmlSchemaForm.Qualified, DataType = "string")]
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public string __Tags;

        [XmlIgnore]
        public string Tags
        {
            get { return __Tags; }
            set { __Tags = value; }
        }

        [XmlElement(ElementName = "Metadata", IsNullable = false, Form = XmlSchemaForm.Qualified, DataType = "string")]
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public string __Metadata;

        [XmlIgnore]
        public string Metadata
        {
            get { return __Metadata; }
            set { __Metadata = value; }
        }

        public FlashMediaInfoStruct()
        {
            Tags = string.Empty;
            Metadata = string.Empty;
        }

        public void MakeSchemaCompliant()
        {
        }
    }


    [XmlType(TypeName = "MediaListTags"), Serializable]
    public partial class MediaListTags
    {

        [XmlElement(ElementName = "Tags", IsNullable = false, Form = XmlSchemaForm.Qualified, DataType = "string")]
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public string __Tags;

        [XmlIgnore]
        public string Tags
        {
            get { return __Tags; }
            set { __Tags = value; }
        }

        public MediaListTags()
        {
            Tags = string.Empty;
        }

        public void MakeSchemaCompliant()
        {
        }
    }


    [XmlType(TypeName = "TagsGallery"), Serializable]
    public partial class TagsGallery
    {

        [XmlElement(ElementName = "Tags", IsNullable = false, Form = XmlSchemaForm.Qualified, DataType = "string")]
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public string __Tags;

        [XmlIgnore]
        public string Tags
        {
            get { return __Tags; }
            set { __Tags = value; }
        }

        public TagsGallery()
        {
            Tags = string.Empty;
        }

        public void MakeSchemaCompliant()
        {
        }
    }


    [XmlType(TypeName = "AutoCompleteValues"), Serializable]
    public partial class AutoCompleteValues
    {

        [XmlElement(ElementName = "Tags", IsNullable = false, Form = XmlSchemaForm.Qualified, DataType = "string")]
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public string __Tags;

        [XmlIgnore]
        public string Tags
        {
            get { return __Tags; }
            set { __Tags = value; }
        }

        [XmlElement(ElementName = "Metadata", IsNullable = false, Form = XmlSchemaForm.Qualified, DataType = "string")]
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public string __Metadata;

        [XmlIgnore]
        public string Metadata
        {
            get { return __Metadata; }
            set { __Metadata = value; }
        }

        [XmlElement(ElementName = "MediaTypeIDs", IsNullable = false, Form = XmlSchemaForm.Qualified, DataType = "string")]
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public string __MediaTypeIDs;

        [XmlIgnore]
        public string MediaTypeIDs
        {
            get { return __MediaTypeIDs; }
            set { __MediaTypeIDs = value; }
        }

        public AutoCompleteValues()
        {
            Tags = string.Empty;
            Metadata = string.Empty;
        }

        public void MakeSchemaCompliant()
        {
        }
    }


    [XmlType(TypeName = "AdvertisingValues"), Serializable]
    public partial class AdvertisingValues
    {

        [XmlElement(ElementName = "Tags", IsNullable = false, Form = XmlSchemaForm.Qualified, DataType = "string")]
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public string __Tags;

        [XmlIgnore]
        public string Tags
        {
            get { return __Tags; }
            set { __Tags = value; }
        }

        [XmlElement(ElementName = "Metas", IsNullable = false, Form = XmlSchemaForm.Qualified, DataType = "string")]
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public string __Metas;

        [XmlIgnore]
        public string Metas
        {
            get { return __Metas; }
            set { __Metas = value; }
        }

        public AdvertisingValues()
        {
            Tags = string.Empty;
            Metas = string.Empty;
        }

        public void MakeSchemaCompliant()
        {
        }
    }
}
