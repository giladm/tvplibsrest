// Copyright 2004, Microsoft Corporation
// Sample Code - Use restricted to terms of use defined in the accompanying license agreement (EULA.doc)

//--------------------------------------------------------------
// Autogenerated by XSDObjectGen version 1.4.4.1
// Schema file: Online.xsd
// Creation Date: 10/05/2010 17:00:30
//--------------------------------------------------------------

using System;
using System.Xml.Serialization;
using System.Collections;
using System.Xml.Schema;
using System.ComponentModel;

namespace TVPPro.Configuration.Online
{

	public struct Declarations
	{
		public const string SchemaVersion = "";
	}

	[Serializable]
	public enum Status
	{
		[XmlEnum(Name="NotDefined")] NotDefined,
		[XmlEnum(Name="On")] On,
		[XmlEnum(Name="Off")] Off
	}

	[Serializable]
	public enum Status2
	{
		[XmlEnum(Name="Off")] Off,
		[XmlEnum(Name="Trial")] Trial,
		[XmlEnum(Name="Maintenance")] Maintenance
	}

	[Serializable]
	public enum TVMMode
	{
		[XmlEnum(Name="Default")] @Default,
		[XmlEnum(Name="Main")] Main,
		[XmlEnum(Name="Alternative")] Alternative
	}


	[Serializable]
	[EditorBrowsable(EditorBrowsableState.Advanced)]
	public partial class ServerCollection : ArrayList
	{
		public TVPPro.Configuration.Online.Server Add(TVPPro.Configuration.Online.Server obj)
		{
			base.Add(obj);
			return obj;
		}

		public TVPPro.Configuration.Online.Server Add()
		{
			return Add(new TVPPro.Configuration.Online.Server());
		}

		public void Insert(int index, TVPPro.Configuration.Online.Server obj)
		{
			base.Insert(index, obj);
		}

		public void Remove(TVPPro.Configuration.Online.Server obj)
		{
			base.Remove(obj);
		}

		new public TVPPro.Configuration.Online.Server this[int index]
		{
			get { return (TVPPro.Configuration.Online.Server) base[index]; }
			set { base[index] = value; }
		}
	}

	[Serializable]
	[EditorBrowsable(EditorBrowsableState.Advanced)]
	public partial class IPCollection : ArrayList
	{
		public string Add(string obj)
		{
			base.Add(obj);
			return obj;
		}

		public void Insert(int index, string obj)
		{
			base.Insert(index, obj);
		}

		public void Remove(string obj)
		{
			base.Remove(obj);
		}

		new public string this[int index]
		{
			get { return (string) base[index]; }
			set { base[index] = value; }
		}
	}



	[XmlType(TypeName="Server"),Serializable]
	[EditorBrowsable(EditorBrowsableState.Advanced)]
	public partial class Server
	{

		[XmlAttribute(AttributeName="ID")]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __ID;
		
		[XmlIgnore]
		public string ID
		{ 
			get { return __ID; }
			set { __ID = value; }
		}

		[XmlElement(ElementName="URL",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string")]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __URL;
		
		[XmlIgnore]
		public string URL
		{ 
			get { return __URL; }
			set { __URL = value; }
		}

		[XmlElement(ElementName="TVMReadURL",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string")]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __TVMReadURL;
		
		[XmlIgnore]
		public string TVMReadURL
		{ 
			get { return __TVMReadURL; }
			set { __TVMReadURL = value; }
		}

		[XmlElement(ElementName="TVMWriteURL",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string")]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __TVMWriteURL;
		
		[XmlIgnore]
		public string TVMWriteURL
		{ 
			get { return __TVMWriteURL; }
			set { __TVMWriteURL = value; }
		}

		public Server()
		{
			URL = string.Empty;
			TVMReadURL = string.Empty;
			TVMWriteURL = string.Empty;
		}

		public void MakeSchemaCompliant()
		{
		}
	}


	[XmlType(TypeName="Mode"),Serializable]
	[EditorBrowsable(EditorBrowsableState.Advanced)]
	public partial class Mode
	{

		[XmlAttribute(AttributeName="Status")]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public Status __Status;
		
		[XmlIgnore]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public bool __StatusSpecified;
		
		[XmlIgnore]
		public Status Status
		{ 
			get { return __Status; }
			set { __Status = value; __StatusSpecified = true; }
		}

		public Mode()
		{
			Status = Status.NotDefined;
		}

		public void MakeSchemaCompliant()
		{
		}
	}


	[XmlRoot(ElementName="OnlineConfiguration",IsNullable=false),Serializable]
	public partial class OnlineConfiguration
	{

		[XmlElement(Type=typeof(Modes),ElementName="Modes",IsNullable=false,Form=XmlSchemaForm.Qualified)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public Modes __Modes;
		
		[XmlIgnore]
		public Modes Modes
		{
			get
			{
				if (__Modes == null) __Modes = new Modes();		
				return __Modes;
			}
			set {__Modes = value;}
		}

		[XmlElement(Type=typeof(TVM),ElementName="TVM",IsNullable=false,Form=XmlSchemaForm.Qualified)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public TVM __TVM;
		
		[XmlIgnore]
		public TVM TVM
		{
			get
			{
				if (__TVM == null) __TVM = new TVM();		
				return __TVM;
			}
			set {__TVM = value;}
		}

		public OnlineConfiguration()
		{
		}

		public void MakeSchemaCompliant()
		{
			Modes.MakeSchemaCompliant();
			TVM.MakeSchemaCompliant();
		}
	}


	[XmlType(TypeName="Modes"),Serializable]
	public partial class Modes
	{

		[XmlElement(Type=typeof(SiteMaintaince),ElementName="SiteMaintaince",IsNullable=false,Form=XmlSchemaForm.Qualified)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public SiteMaintaince __SiteMaintaince;
		
		[XmlIgnore]
		public SiteMaintaince SiteMaintaince
		{
			get
			{
				if (__SiteMaintaince == null) __SiteMaintaince = new SiteMaintaince();		
				return __SiteMaintaince;
			}
			set {__SiteMaintaince = value;}
		}

		[XmlElement(Type=typeof(TVPPro.Configuration.Online.Mode),ElementName="ViewMedia",IsNullable=false,Form=XmlSchemaForm.Qualified)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public TVPPro.Configuration.Online.Mode __ViewMedia;
		
		[XmlIgnore]
		public TVPPro.Configuration.Online.Mode ViewMedia
		{
			get
			{
				if (__ViewMedia == null) __ViewMedia = new TVPPro.Configuration.Online.Mode();		
				return __ViewMedia;
			}
			set {__ViewMedia = value;}
		}

		[XmlElement(Type=typeof(TVPPro.Configuration.Online.Mode),ElementName="PurchaseMedia",IsNullable=false,Form=XmlSchemaForm.Qualified)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public TVPPro.Configuration.Online.Mode __PurchaseMedia;
		
		[XmlIgnore]
		public TVPPro.Configuration.Online.Mode PurchaseMedia
		{
			get
			{
				if (__PurchaseMedia == null) __PurchaseMedia = new TVPPro.Configuration.Online.Mode();		
				return __PurchaseMedia;
			}
			set {__PurchaseMedia = value;}
		}

		public Modes()
		{
		}

		public void MakeSchemaCompliant()
		{
			SiteMaintaince.MakeSchemaCompliant();
			ViewMedia.MakeSchemaCompliant();
			PurchaseMedia.MakeSchemaCompliant();
		}
	}


	[XmlType(TypeName="SiteMaintaince"),Serializable]
	public partial class SiteMaintaince
	{

		[XmlAttribute(AttributeName="Status")]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public Status2 __Status;
		
		[XmlIgnore]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public bool __StatusSpecified;
		
		[XmlIgnore]
		public Status2 Status
		{ 
			get { return __Status; }
			set { __Status = value; __StatusSpecified = true; }
		}

		public SiteMaintaince()
		{
			__StatusSpecified = true;
		}

		public void MakeSchemaCompliant()
		{
		}
	}


	[XmlType(TypeName="TVM"),Serializable]
	public partial class TVM
	{

		[XmlAttribute(AttributeName="TVMMode")]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public TVMMode __TVMMode;
		
		[XmlIgnore]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public bool __TVMModeSpecified;
		
		[XmlIgnore]
		public TVMMode TVMMode
		{ 
			get { return __TVMMode; }
			set { __TVMMode = value; __TVMModeSpecified = true; }
		}

		[XmlElement(Type=typeof(AlternativeServer),ElementName="AlternativeServer",IsNullable=false,Form=XmlSchemaForm.Qualified)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public AlternativeServer __AlternativeServer;
		
		[XmlIgnore]
		public AlternativeServer AlternativeServer
		{
			get
			{
				if (__AlternativeServer == null) __AlternativeServer = new AlternativeServer();		
				return __AlternativeServer;
			}
			set {__AlternativeServer = value;}
		}

		[XmlElement(Type=typeof(TVPPro.Configuration.Online.Server),ElementName="MainServer",IsNullable=false,Form=XmlSchemaForm.Qualified)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public TVPPro.Configuration.Online.Server __MainServer;
		
		[XmlIgnore]
		public TVPPro.Configuration.Online.Server MainServer
		{
			get
			{
				if (__MainServer == null) __MainServer = new TVPPro.Configuration.Online.Server();		
				return __MainServer;
			}
			set {__MainServer = value;}
		}

		[XmlElement(Type=typeof(AuthorizedServers),ElementName="AuthorizedServers",IsNullable=false,Form=XmlSchemaForm.Qualified)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public AuthorizedServers __AuthorizedServers;
		
		[XmlIgnore]
		public AuthorizedServers AuthorizedServers
		{
			get
			{
				if (__AuthorizedServers == null) __AuthorizedServers = new AuthorizedServers();		
				return __AuthorizedServers;
			}
			set {__AuthorizedServers = value;}
		}

		public TVM()
		{
			TVMMode = TVMMode.Default;
		}

		public void MakeSchemaCompliant()
		{
			MainServer.MakeSchemaCompliant();
			AuthorizedServers.MakeSchemaCompliant();
		}
	}


	[XmlType(TypeName="AlternativeServer"),Serializable]
	public partial class AlternativeServer
	{
		[System.Runtime.InteropServices.DispIdAttribute(-4)]
		public IEnumerator GetEnumerator() 
		{
            return ItemCollection.GetEnumerator();
		}

		public TVPPro.Configuration.Online.Server Add(TVPPro.Configuration.Online.Server obj)
		{
			return ItemCollection.Add(obj);
		}

		[XmlIgnore]
		public TVPPro.Configuration.Online.Server this[int index]
		{
			get { return (TVPPro.Configuration.Online.Server) ItemCollection[index]; }
		}

		[XmlIgnore]
        public int Count 
		{
            get { return ItemCollection.Count; }
        }

        public void Clear()
		{
			ItemCollection.Clear();
        }

		public TVPPro.Configuration.Online.Server Remove(int index) 
		{ 
            TVPPro.Configuration.Online.Server obj = ItemCollection[index];
            ItemCollection.Remove(obj);
			return obj;
        }

        public void Remove(object obj)
		{
            ItemCollection.Remove(obj);
        }

		[XmlAttribute(AttributeName="ActiveID",DataType="string")]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __ActiveID;
		
		[XmlIgnore]
		public string ActiveID
		{ 
			get { return __ActiveID; }
			set { __ActiveID = value; }
		}

		[XmlElement(Type=typeof(TVPPro.Configuration.Online.Server),ElementName="Item",IsNullable=false,Form=XmlSchemaForm.Qualified)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public ServerCollection __ItemCollection;
		
		[XmlIgnore]
		public ServerCollection ItemCollection
		{
			get
			{
				if (__ItemCollection == null) __ItemCollection = new ServerCollection();
				return __ItemCollection;
			}
			set {__ItemCollection = value;}
		}

		public AlternativeServer()
		{
		}

		public void MakeSchemaCompliant()
		{
		}
	}


	[XmlType(TypeName="AuthorizedServers"),Serializable]
	public partial class AuthorizedServers
	{
		[System.Runtime.InteropServices.DispIdAttribute(-4)]
		public IEnumerator GetEnumerator() 
		{
            return IPCollection.GetEnumerator();
		}

		public string Add(string obj)
		{
			return IPCollection.Add(obj);
		}

		[XmlIgnore]
		public string this[int index]
		{
			get { return (string) IPCollection[index]; }
		}

		[XmlIgnore]
        public int Count 
		{
            get { return IPCollection.Count; }
        }

        public void Clear()
		{
			IPCollection.Clear();
        }

		public string Remove(int index) 
		{ 
            string obj = IPCollection[index];
            IPCollection.Remove(obj);
			return obj;
        }

        public void Remove(object obj)
		{
            IPCollection.Remove(obj);
        }

		[XmlElement(Type=typeof(string),ElementName="IP",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string")]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public IPCollection __IPCollection;
		
		[XmlIgnore]
		public IPCollection IPCollection
		{
			get
			{
				if (__IPCollection == null) __IPCollection = new IPCollection();
				return __IPCollection;
			}
			set {__IPCollection = value;}
		}

		public AuthorizedServers()
		{
		}

		public void MakeSchemaCompliant()
		{
			if (IPCollection.Count == 0) IPCollection.Add("");
		}
	}
}
