﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tvinci.Configuration;
using System.Threading;
using System.Configuration;
using log4net;

namespace TVPPro.Configuration.PageStruct
{
    public class PageConfiguration : ConfigurationManager<PageStruct>
    {
        private static ILog logger = log4net.LogManager.GetLogger(typeof(PageConfiguration));

        ReaderWriterLockSlim m_locker = new ReaderWriterLockSlim();

        static PageConfiguration instance = null;
        static object instanceLock = new object();

        private PageConfiguration()
		{
			base.SyncFromFile(ConfigurationManager.AppSettings["TVPPro.Configuration.PageStruct"], true);
            m_syncFile = ConfigurationManager.AppSettings["TVPPro.Configuration.PageStruct"];
		}

        private PageConfiguration(string syncFile)
        {
            base.SyncFromFile(syncFile, true);
            m_syncFile = syncFile;
        }

        public static PageConfiguration Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (instanceLock)
                    {
                        if (instance == null)
                        {
                            instance = new PageConfiguration();
                        }
                    }
                }

                return instance;
            }
        }

        public static PageConfiguration GetInstance(string syncFile)
        {
            if (instance == null)
            {
                lock (instanceLock)
                {
                    if (instance == null)
                    {
                        instance = new PageConfiguration(syncFile);
                    }
                    else
                    {
                        lock (instanceLock)
                        {
                            instance.ReSyncFromFile(syncFile);
                        }
                    }
                }
            }
            else
            {
                lock (instanceLock)
                {
                    instance.ReSyncFromFile(syncFile);
                }
            }

            return instance;
        }
    }
}
