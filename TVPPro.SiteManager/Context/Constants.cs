﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TVPPro.SiteManager.Context
{
	public static class CacheKeys
	{
		public const string ComponentTableKey = "ComponentTableKey";
	}

	public static class GalleryConstants
	{
		public const string PagingCommandKey = "Paging";

	}

	public enum MediaFinderLocation
	{
		Top,
		Side
	}
}
