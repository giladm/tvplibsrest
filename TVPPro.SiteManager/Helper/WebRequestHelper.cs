﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace TVPPro.SiteManager.Helper
{
    public class WebRequestHelper
    {
        public static T SendRequest<T>(string url, string postData)
        {
            Stream dataStream = null;
            StreamReader reader = null;
            WebResponse response = null;
            try
            {
                WebRequest request = WebRequest.Create(url);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";

                byte[] byteArray = Encoding.UTF8.GetBytes(postData);

                request.ContentLength = byteArray.Length;

                dataStream = request.GetRequestStream();
                dataStream.Write(byteArray, 0, byteArray.Length);
                dataStream.Close();

                response = request.GetResponse();
                dataStream = response.GetResponseStream();

                reader = new StreamReader(dataStream);
                System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();

                if (typeof(T) == typeof(String))
                {
                    string resposne = reader.ReadToEnd().Trim(new char[] { '\"' });
                    return (T)Convert.ChangeType(resposne, typeof(T));
                }

                return serializer.Deserialize<T>(reader.ReadToEnd());

            }
            finally
            {
                // Clean up the streams.
                if (reader != null) reader.Close();
                if (dataStream != null) dataStream.Close();
                if (response != null) response.Close();
            }
        }
    }
}
