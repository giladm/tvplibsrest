﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TVPPro.SiteManager.Objects
{
   public class MediaCorpBuildingEntity
    {
        public string BuildingKey { get; set; }
        public string BuildingName { get; set; }
        public char TypeFlag { get; set; }
    }
}
