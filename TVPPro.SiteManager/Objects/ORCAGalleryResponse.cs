﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TVPPro.SiteManager.Objects
{
    public class ORCAGalleryResponse
    {
        public int ContentType { get; set; }
        public object[] Content { get; set; }
    }
}
