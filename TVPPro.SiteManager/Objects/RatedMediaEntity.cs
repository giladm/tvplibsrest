﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TVPPro.SiteManager.Objects
{
    public class RatedMediaEntity
    {
        public string MediaID { get; set; }
        public int Score { get; set; }
    }
}
