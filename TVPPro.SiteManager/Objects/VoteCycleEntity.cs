﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TVPPro.SiteManager.Objects
{
    public class VoteCycleEntity
    {
        public DateTime CreateDate { get; set; }
        public DateTime ResetDate { get; set; }
        public string ID { get; set; }
    }
}
