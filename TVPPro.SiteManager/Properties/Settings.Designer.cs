﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18444
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TVPPro.SiteManager.Properties {
    
    
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "11.0.0.0")]
    internal sealed partial class Settings : global::System.Configuration.ApplicationSettingsBase {
        
        private static Settings defaultInstance = ((Settings)(global::System.Configuration.ApplicationSettingsBase.Synchronized(new Settings())));
        
        public static Settings Default {
            get {
                return defaultInstance;
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("https://platform.tvinci.com/users/module.asmx")]
        public string TVPPro_SiteManager_Tvinci_Users_UsersService {
            get {
                return ((string)(this["TVPPro_SiteManager_Tvinci_Users_UsersService"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://webservices.stg.tvincidns.com/cas/module.asmx")]
        public string TVPPro_SiteManager_TVPPro_SiteManager_TvinciPlatform_ConditionalAccess_module {
            get {
                return ((string)(this["TVPPro_SiteManager_TVPPro_SiteManager_TvinciPlatform_ConditionalAccess_module"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://webservices-us.tvinci.com/billing_v1_3/module.asmx")]
        public string TVPPro_SiteManager_TvinciPlatform_Billing_module {
            get {
                return ((string)(this["TVPPro_SiteManager_TvinciPlatform_Billing_module"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://192.168.192.146/webservices/pricing/module.asmx")]
        public string TVPPro_SiteManager_TvinciPlatform_Pricing_mdoule {
            get {
                return ((string)(this["TVPPro_SiteManager_TvinciPlatform_Pricing_mdoule"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://192.168.16.176/webservices/users/module.asmx")]
        public string TVPPro_SiteManager_TvinciPlatform_Users_UsersService {
            get {
                return ((string)(this["TVPPro_SiteManager_TvinciPlatform_Users_UsersService"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://192.168.192.146/webservices/domains/module.asmx")]
        public string TVPPro_SiteManager_TvinciPlatform_Domains_module {
            get {
                return ((string)(this["TVPPro_SiteManager_TvinciPlatform_Domains_module"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://192.168.192.146/webservices/social/module.asmx")]
        public string TVPPro_SiteManager_TvinciPlatform_Social_module {
            get {
                return ((string)(this["TVPPro_SiteManager_TvinciPlatform_Social_module"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://192.168.192.146/webservices/cas/module.asmx")]
        public string TVPPro_SiteManager_TvinciPlatform_ConditionalAccess_module {
            get {
                return ((string)(this["TVPPro_SiteManager_TvinciPlatform_ConditionalAccess_module"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://192.168.192.146/webservices/api/api.asmx")]
        public string TVPPro_SiteManager_TvinciPlatform_api_API {
            get {
                return ((string)(this["TVPPro_SiteManager_TvinciPlatform_api_API"]));
            }
        }
    }
}
