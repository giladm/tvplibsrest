﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tvinci.Data.Loaders.TvinciPlatform.Catalog;
using System.Net;
using log4net;
using System.Xml.Serialization;
using System.IO;
using System.Xml;
using TVPPro.Configuration.PlatformServices;


namespace Tvinci.Data.Loaders
{
    [Serializable]
    public class TVMCatalogProvider : Provider
    {
        private static ILog logger = log4net.LogManager.GetLogger(typeof(TVMCatalogProvider));        
        private static TvinciPlatform.Catalog.IserviceClient m_oClient = null;
        private string m_EndPoint;

        public TVMCatalogProvider(string endPointAddress)
        {
            if (!string.IsNullOrEmpty(endPointAddress))
            {                
                if (m_oClient == null)
                {
                    try
                    {
                        m_EndPoint = endPointAddress;
                        m_oClient = new IserviceClient(string.Empty, m_EndPoint);
                    }
                    catch (Exception ex)
                    {
                        logger.Error("Exception on Catalog Client creation", ex);
                        m_oClient = null;
                    }

                    FailOverManager.Instance.SafeModeStarted += () => { if (m_oClient != null) m_oClient.Close(); m_oClient = null; };
                    FailOverManager.Instance.SafeModeEnded += () => { if (m_oClient == null) m_oClient = new IserviceClient(string.Empty, m_EndPoint); };
                }
            }            
        }

        public override eProviderResult TryExecuteGetMediasByIDs(MediasProtocolRequest request, out MediaResponse response)
        {            
            try
            {
                if (!FailOverManager.Instance.SafeMode)
                {
                    DateTime start = DateTime.Now;                    
                    response = m_oClient.GetMediasByIDs(request);
                    TimeSpan span = DateTime.Now - start;
                    
                    logger.InfoFormat("TryExecuteGetMediasByIDs-> Total time: {0}ms", span.TotalMilliseconds);                

                    FailOverManager.Instance.AddRequest(true);
                    return eProviderResult.Success;
                }
                else
                {
                    response = null;
                    return eProviderResult.SafeMode;
                }
            }
            catch (Exception ex)
            {
                if (!FailOverManager.Instance.SafeMode)
                    FailOverManager.Instance.AddRequest(false);

                logger.Error("Exception in CatalogGetMediasByIDs", ex);
                response = null;
                if (ex is TimeoutException)
                    return eProviderResult.TimeOut;
                return eProviderResult.Fail;
            }           

        }

        public override eProviderResult TryExecuteGetBaseResponse(BaseRequest request, out BaseResponse response)
        {            
            try
            {
                if (!FailOverManager.Instance.SafeMode)
                {
                    DateTime start = DateTime.Now;
                    response = m_oClient.GetResponse(request);
                    TimeSpan span = DateTime.Now - start;

                    logger.InfoFormat("TryExecuteGetMediasByIDs-> Total time: {0}ms", span.TotalMilliseconds);

                    FailOverManager.Instance.AddRequest(true);
                    return eProviderResult.Success;
                }
                else
                {
                    response = null;
                    return eProviderResult.SafeMode;
                }
            }
            catch (Exception ex)
            {
                if (!FailOverManager.Instance.SafeMode)
                    FailOverManager.Instance.AddRequest(false);
                logger.Error("Exception in CatalogGetBaseRsponse", ex);
                response = null;
                if (ex is TimeoutException)
                    return eProviderResult.TimeOut;
                return eProviderResult.Fail;
            }          
        }

        public override eProviderResult TryExecuteGetProgramsByIDs(EpgProgramDetailsRequest request, out EpgProgramResponse response)
        {            
            try
            {
                if (!FailOverManager.Instance.SafeMode)
                {
                    DateTime start = DateTime.Now;
                    response = m_oClient.GetProgramsByIDs(request);
                    TimeSpan span = DateTime.Now - start;

                    logger.InfoFormat("TryExecuteGetMediasByIDs-> Total time: {0}ms", span.TotalMilliseconds);

                    FailOverManager.Instance.AddRequest(true);
                    return eProviderResult.Success;
                }
                else
                {
                    response = null;
                    return eProviderResult.SafeMode;
                }
            }
            catch (Exception ex)
            {
                if (!FailOverManager.Instance.SafeMode)
                    FailOverManager.Instance.AddRequest(false);
                logger.Error("Exception in TryExecuteGetProgramsByIDs", ex);
                response = null;
                if (ex is TimeoutException)
                    return eProviderResult.TimeOut;
                return eProviderResult.Fail;
            }          
        }
    }
}
