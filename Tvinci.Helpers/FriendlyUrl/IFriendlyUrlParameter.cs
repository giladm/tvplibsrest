﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tvinci.Helpers.FriendlyUrl
{
    public interface IFriendlyUrlParameter
    {
        string ProviderID { get; }
    }
}
