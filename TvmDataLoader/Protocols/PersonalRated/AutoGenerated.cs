// Copyright 2004, Microsoft Corporation
// Sample Code - Use restricted to terms of use defined in the accompanying license agreement (EULA.doc)

//--------------------------------------------------------------
// Autogenerated by XSDObjectGen version 1.4.4.1
// Schema file: PersonalRated.xsd
// Creation Date: 11/30/2008 3:14:20 PM
//--------------------------------------------------------------

using System;
using System.Xml.Serialization;
using System.Collections;
using System.Xml.Schema;
using System.ComponentModel;

namespace Tvinci.Data.TVMDataLoader.Protocols.PersonalRated
{

	public struct Declarations
	{
		public const string SchemaVersion = "";
	}

	[Serializable]
	public enum file_quality
	{
		[XmlEnum(Name="low")] low,
		[XmlEnum(Name="medium")] medium,
		[XmlEnum(Name="high")] high
	}


	[Serializable]
	[EditorBrowsable(EditorBrowsableState.Advanced)]
	public partial class mediaCollection : ArrayList
	{
		public media Add(media obj)
		{
			base.Add(obj);
			return obj;
		}

		public media Add()
		{
			return Add(new media());
		}

		public void Insert(int index, media obj)
		{
			base.Insert(index, obj);
		}

		public void Remove(media obj)
		{
			base.Remove(obj);
		}

		new public media this[int index]
		{
			get { return (media) base[index]; }
			set { base[index] = value; }
		}
	}



	[XmlRoot(ElementName="PersonalRated",IsNullable=false),Serializable]
	public partial class PersonalRated
	{

		[XmlElement(Type=typeof(root),ElementName="root",IsNullable=false,Form=XmlSchemaForm.Qualified)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public root __root;
		
		[XmlIgnore]
		public root root
		{
			get
			{
				if (__root == null) __root = new root();		
				return __root;
			}
			set {__root = value;}
		}

		[XmlElement(Type=typeof(response),ElementName="response",IsNullable=false,Form=XmlSchemaForm.Qualified)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public response __response;
		
		[XmlIgnore]
		public response response
		{
			get
			{
				if (__response == null) __response = new response();		
				return __response;
			}
			set {__response = value;}
		}

		public PersonalRated()
		{
		}

		public void MakeSchemaCompliant()
		{
			root.MakeSchemaCompliant();
			response.MakeSchemaCompliant();
		}
	}


	[XmlType(TypeName="root"),Serializable]
	public partial class root
	{

		[XmlElement(Type=typeof(request),ElementName="request",IsNullable=false,Form=XmlSchemaForm.Qualified)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public request __request;
		
		[XmlIgnore]
		public request request
		{
			get
			{
				if (__request == null) __request = new request();		
				return __request;
			}
			set {__request = value;}
		}

		[XmlElement(Type=typeof(flashvars),ElementName="flashvars",IsNullable=false,Form=XmlSchemaForm.Qualified)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public flashvars __flashvars;
		
		[XmlIgnore]
		public flashvars flashvars
		{
			get
			{
				if (__flashvars == null) __flashvars = new flashvars();		
				return __flashvars;
			}
			set {__flashvars = value;}
		}

		public root()
		{
		}

		public void MakeSchemaCompliant()
		{
			request.MakeSchemaCompliant();
			flashvars.MakeSchemaCompliant();
		}
	}


	[XmlType(TypeName="request"),Serializable]
	public partial class request
	{

		[XmlAttribute(AttributeName="type")]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __type;
		
		[XmlIgnore]
		public string type
		{ 
			get { return __type; }
			set { __type = value; }
		}

		[XmlElement(Type=typeof(@params),ElementName="params",IsNullable=false,Form=XmlSchemaForm.Qualified)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public @params __params;
		
		[XmlIgnore]
		public @params @params
		{
			get
			{
				if (__params == null) __params = new @params();		
				return __params;
			}
			set {__params = value;}
		}

		[XmlElement(Type=typeof(rate),ElementName="rate",IsNullable=false,Form=XmlSchemaForm.Qualified)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public rate __rate;
		
		[XmlIgnore]
		public rate rate
		{
			get
			{
				if (__rate == null) __rate = new rate();		
				return __rate;
			}
			set {__rate = value;}
		}

		[XmlElement(Type=typeof(channel),ElementName="channel",IsNullable=false,Form=XmlSchemaForm.Qualified)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public channel __channel;
		
		[XmlIgnore]
		public channel channel
		{
			get
			{
				if (__channel == null) __channel = new channel();		
				return __channel;
			}
			set {__channel = value;}
		}

		public request()
		{
			type = "personal_rated";
		}

		public void MakeSchemaCompliant()
		{
			@params.MakeSchemaCompliant();
			rate.MakeSchemaCompliant();
			channel.MakeSchemaCompliant();
		}
	}


	[XmlType(TypeName="params"),Serializable]
	public partial class @params
	{

		[XmlAttribute(AttributeName="with_info",DataType="string")]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __with_info;
		
		[XmlIgnore]
		public string with_info
		{ 
			get { return __with_info; }
			set { __with_info = value; }
		}

		public @params()
		{
			with_info = "false";
		}

		public void MakeSchemaCompliant()
		{
		}
	}


	[XmlType(TypeName="rate"),Serializable]
	public partial class rate
	{

		[XmlAttribute(AttributeName="min",DataType="int")]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public int __min;
		
		[XmlIgnore]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public bool __minSpecified;
		
		[XmlIgnore]
		public int min
		{ 
			get { return __min; }
			set { __min = value; __minSpecified = true; }
		}

		[XmlAttribute(AttributeName="max",DataType="int")]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public int __max;
		
		[XmlIgnore]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public bool __maxSpecified;
		
		[XmlIgnore]
		public int max
		{ 
			get { return __max; }
			set { __max = value; __maxSpecified = true; }
		}

		[XmlAttribute(AttributeName="site_guid",DataType="string")]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __site_guid;
		
		[XmlIgnore]
		public string site_guid
		{ 
			get { return __site_guid; }
			set { __site_guid = value; }
		}

		public rate()
		{
		}

		public void MakeSchemaCompliant()
		{
		}
	}


	[XmlType(TypeName="channel"),Serializable]
	public partial class channel
	{

		[XmlAttribute(AttributeName="id",DataType="int")]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public int __id;
		
		[XmlIgnore]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public bool __idSpecified;
		
		[XmlIgnore]
		public int id
		{ 
			get { return __id; }
			set { __id = value; __idSpecified = true; }
		}

		[XmlAttribute(AttributeName="start_index",DataType="int")]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public int __start_index;
		
		[XmlIgnore]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public bool __start_indexSpecified;
		
		[XmlIgnore]
		public int start_index
		{ 
			get { return __start_index; }
			set { __start_index = value; __start_indexSpecified = true; }
		}

		[XmlAttribute(AttributeName="number_of_items",DataType="int")]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public int __number_of_items;
		
		[XmlIgnore]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public bool __number_of_itemsSpecified;
		
		[XmlIgnore]
		public int number_of_items
		{ 
			get { return __number_of_items; }
			set { __number_of_items = value; __number_of_itemsSpecified = true; }
		}

		public channel()
		{
			__idSpecified = true;
			__start_indexSpecified = true;
			__number_of_itemsSpecified = true;
		}

		public void MakeSchemaCompliant()
		{
		}
	}


	[XmlType(TypeName="flashvars"),Serializable]
	public partial class flashvars
	{

		[XmlAttribute(AttributeName="player_un")]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __player_un;
		
		[XmlIgnore]
		public string player_un
		{ 
			get { return __player_un; }
			set { __player_un = value; }
		}

		[XmlAttribute(AttributeName="player_pass")]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __player_pass;
		
		[XmlIgnore]
		public string player_pass
		{ 
			get { return __player_pass; }
			set { __player_pass = value; }
		}

		[XmlAttribute(AttributeName="pic_size1")]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __pic_size1;
		
		[XmlIgnore]
		public string pic_size1
		{ 
			get { return __pic_size1; }
			set { __pic_size1 = value; }
		}

		[XmlAttribute(AttributeName="lang")]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __lang;
		
		[XmlIgnore]
		public string lang
		{ 
			get { return __lang; }
			set { __lang = value; }
		}

		[XmlAttribute(AttributeName="no_cache")]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __no_cache;
		
		[XmlIgnore]
		public string no_cache
		{ 
			get { return __no_cache; }
			set { __no_cache = value; }
		}

		[XmlAttribute(AttributeName="timer")]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __timer;
		
		[XmlIgnore]
		public string timer
		{ 
			get { return __timer; }
			set { __timer = value; }
		}

		[XmlAttribute(AttributeName="file_format")]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __file_format;
		
		[XmlIgnore]
		public string file_format
		{ 
			get { return __file_format; }
			set { __file_format = value; }
		}

		[XmlAttribute(AttributeName="file_quality")]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public file_quality __file_quality;
		
		[XmlIgnore]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public bool __file_qualitySpecified;
		
		[XmlIgnore]
		public file_quality file_quality
		{ 
			get { return __file_quality; }
			set { __file_quality = value; __file_qualitySpecified = true; }
		}

		public flashvars()
		{
			player_un = string.Empty;
			player_pass = string.Empty;
			pic_size1 = string.Empty;
		}

		public void MakeSchemaCompliant()
		{
		}
	}


	[XmlType(TypeName="response"),Serializable]
	public partial class response
	{

		[XmlAttribute(AttributeName="type")]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __type;
		
		[XmlIgnore]
		public string type
		{ 
			get { return __type; }
			set { __type = value; }
		}

		[XmlElement(Type=typeof(playlist_schema),ElementName="playlist_schema",IsNullable=false,Form=XmlSchemaForm.Qualified)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public playlist_schema __playlist_schema;
		
		[XmlIgnore]
		public playlist_schema playlist_schema
		{
			get
			{
				if (__playlist_schema == null) __playlist_schema = new playlist_schema();		
				return __playlist_schema;
			}
			set {__playlist_schema = value;}
		}

		[XmlElement(Type=typeof(responsechannel),ElementName="channel",IsNullable=false,Form=XmlSchemaForm.Qualified)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public responsechannel __channel;
		
		[XmlIgnore]
		public responsechannel channel
		{
			get
			{
				if (__channel == null) __channel = new responsechannel();		
				return __channel;
			}
			set {__channel = value;}
		}

		public response()
		{
		}

		public void MakeSchemaCompliant()
		{
			playlist_schema.MakeSchemaCompliant();
			channel.MakeSchemaCompliant();
		}
	}


	[XmlType(TypeName="playlist_schema"),Serializable]
	public partial class playlist_schema
	{

		public playlist_schema()
		{
		}

		public void MakeSchemaCompliant()
		{
		}
	}


	[XmlType(TypeName="responsechannel"),Serializable]
	public partial class responsechannel
	{
		[System.Runtime.InteropServices.DispIdAttribute(-4)]
		public IEnumerator GetEnumerator() 
		{
            return mediaCollection.GetEnumerator();
		}

		public media Add(media obj)
		{
			return mediaCollection.Add(obj);
		}

		[XmlIgnore]
		public media this[int index]
		{
			get { return (media) mediaCollection[index]; }
		}

		[XmlIgnore]
        public int Count 
		{
            get { return mediaCollection.Count; }
        }

        public void Clear()
		{
			mediaCollection.Clear();
        }

		public media Remove(int index) 
		{ 
            media obj = mediaCollection[index];
            mediaCollection.Remove(obj);
			return obj;
        }

        public void Remove(object obj)
		{
            mediaCollection.Remove(obj);
        }

		[XmlAttribute(AttributeName="id")]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __id;
		
		[XmlIgnore]
		public string id
		{ 
			get { return __id; }
			set { __id = value; }
		}

		[XmlAttribute(AttributeName="media_count",DataType="long")]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public long __media_count;
		
		[XmlIgnore]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public bool __media_countSpecified;
		
		[XmlIgnore]
		public long media_count
		{ 
			get { return __media_count; }
			set { __media_count = value; __media_countSpecified = true; }
		}

		[XmlElement(Type=typeof(media),ElementName="media",IsNullable=false,Form=XmlSchemaForm.Qualified)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public mediaCollection __mediaCollection;
		
		[XmlIgnore]
		public mediaCollection mediaCollection
		{
			get
			{
				if (__mediaCollection == null) __mediaCollection = new mediaCollection();
				return __mediaCollection;
			}
			set {__mediaCollection = value;}
		}

		public responsechannel()
		{
		}

		public void MakeSchemaCompliant()
		{
		}
	}


	[XmlType(TypeName="media"),Serializable]
	public partial class media
	{

		[XmlAttribute(AttributeName="pic_size1")]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __pic_size1;
		
		[XmlIgnore]
		public string pic_size1
		{ 
			get { return __pic_size1; }
			set { __pic_size1 = value; }
		}

		[XmlAttribute(AttributeName="title")]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __title;
		
		[XmlIgnore]
		public string title
		{ 
			get { return __title; }
			set { __title = value; }
		}

		[XmlAttribute(AttributeName="id")]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __id;
		
		[XmlIgnore]
		public string id
		{ 
			get { return __id; }
			set { __id = value; }
		}

		public media()
		{
		}

		public void MakeSchemaCompliant()
		{
		}
	}
}
