// Copyright 2004, Microsoft Corporation
// Sample Code - Use restricted to terms of use defined in the accompanying license agreement (EULA.doc)

//--------------------------------------------------------------
// Autogenerated by XSDObjectGen version 1.4.4.1
// Schema file: GroupsMedia.xsd
// Creation Date: 14/10/2012 16:16:52
//--------------------------------------------------------------

using System;
using System.Xml.Serialization;
using System.Collections;
using System.Xml.Schema;
using System.ComponentModel;

namespace Tvinci.Data.TVMDataLoader.Protocols.GroupsMedia
{

	public struct Declarations
	{
		public const string SchemaVersion = "";
	}


	[Serializable]
	[EditorBrowsable(EditorBrowsableState.Advanced)]
	public partial class FileCollection : ArrayList
	{
		public Tvinci.Data.TVMDataLoader.Protocols.GroupsMedia.File Add(Tvinci.Data.TVMDataLoader.Protocols.GroupsMedia.File obj)
		{
			base.Add(obj);
			return obj;
		}

		public Tvinci.Data.TVMDataLoader.Protocols.GroupsMedia.File Add()
		{
			return Add(new Tvinci.Data.TVMDataLoader.Protocols.GroupsMedia.File());
		}

		public void Insert(int index, Tvinci.Data.TVMDataLoader.Protocols.GroupsMedia.File obj)
		{
			base.Insert(index, obj);
		}

		public void Remove(Tvinci.Data.TVMDataLoader.Protocols.GroupsMedia.File obj)
		{
			base.Remove(obj);
		}

		new public Tvinci.Data.TVMDataLoader.Protocols.GroupsMedia.File this[int index]
		{
			get { return (Tvinci.Data.TVMDataLoader.Protocols.GroupsMedia.File) base[index]; }
			set { base[index] = value; }
		}
	}

	[Serializable]
	[EditorBrowsable(EditorBrowsableState.Advanced)]
	public partial class MediaCollection : ArrayList
	{
		public Tvinci.Data.TVMDataLoader.Protocols.GroupsMedia.Media Add(Tvinci.Data.TVMDataLoader.Protocols.GroupsMedia.Media obj)
		{
			base.Add(obj);
			return obj;
		}

		public Tvinci.Data.TVMDataLoader.Protocols.GroupsMedia.Media Add()
		{
			return Add(new Tvinci.Data.TVMDataLoader.Protocols.GroupsMedia.Media());
		}

		public void Insert(int index, Tvinci.Data.TVMDataLoader.Protocols.GroupsMedia.Media obj)
		{
			base.Insert(index, obj);
		}

		public void Remove(Tvinci.Data.TVMDataLoader.Protocols.GroupsMedia.Media obj)
		{
			base.Remove(obj);
		}

		new public Tvinci.Data.TVMDataLoader.Protocols.GroupsMedia.Media this[int index]
		{
			get { return (Tvinci.Data.TVMDataLoader.Protocols.GroupsMedia.Media) base[index]; }
			set { base[index] = value; }
		}
	}



	[XmlType(TypeName="File"),Serializable]
	[EditorBrowsable(EditorBrowsableState.Advanced)]
	public partial class File
	{

		[XmlAttribute(AttributeName="Type",Form=XmlSchemaForm.Unqualified,DataType="string")]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __Type;
		
		[XmlIgnore]
		public string Type
		{ 
			get { return __Type; }
			set { __Type = value; }
		}

		[XmlAttribute(AttributeName="ID",Form=XmlSchemaForm.Unqualified,DataType="int")]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public int __ID;
		
		[XmlIgnore]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public bool __IDSpecified;
		
		[XmlIgnore]
		public int ID
		{ 
			get { return __ID; }
			set { __ID = value; __IDSpecified = true; }
		}

		[XmlAttribute(AttributeName="URL",Form=XmlSchemaForm.Unqualified,DataType="string")]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __URL;
		
		[XmlIgnore]
		public string URL
		{ 
			get { return __URL; }
			set { __URL = value; }
		}

		[XmlAttribute(AttributeName="BillingType",Form=XmlSchemaForm.Unqualified,DataType="string")]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __BillingType;
		
		[XmlIgnore]
		public string BillingType
		{ 
			get { return __BillingType; }
			set { __BillingType = value; }
		}

		[XmlAttribute(AttributeName="is_active",Form=XmlSchemaForm.Unqualified,DataType="int")]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public int __is_active;
		
		[XmlIgnore]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public bool __is_activeSpecified;
		
		[XmlIgnore]
		public int is_active
		{ 
			get { return __is_active; }
			set { __is_active = value; __is_activeSpecified = true; }
		}

		[XmlAttribute(AttributeName="TypeID",Form=XmlSchemaForm.Unqualified,DataType="int")]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public int __TypeID;
		
		[XmlIgnore]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public bool __TypeIDSpecified;
		
		[XmlIgnore]
		public int TypeID
		{ 
			get { return __TypeID; }
			set { __TypeID = value; __TypeIDSpecified = true; }
		}

		public File()
		{
		}

		public void MakeSchemaCompliant()
		{
		}
	}


	[XmlType(TypeName="Media"),Serializable]
	[EditorBrowsable(EditorBrowsableState.Advanced)]
	public partial class Media
	{
		[System.Runtime.InteropServices.DispIdAttribute(-4)]
		public IEnumerator GetEnumerator() 
		{
            return FileCollection.GetEnumerator();
		}

		public Tvinci.Data.TVMDataLoader.Protocols.GroupsMedia.File Add(Tvinci.Data.TVMDataLoader.Protocols.GroupsMedia.File obj)
		{
			return FileCollection.Add(obj);
		}

		[XmlIgnore]
		public Tvinci.Data.TVMDataLoader.Protocols.GroupsMedia.File this[int index]
		{
			get { return (Tvinci.Data.TVMDataLoader.Protocols.GroupsMedia.File) FileCollection[index]; }
		}

		[XmlIgnore]
        public int Count 
		{
            get { return FileCollection.Count; }
        }

        public void Clear()
		{
			FileCollection.Clear();
        }

		public Tvinci.Data.TVMDataLoader.Protocols.GroupsMedia.File Remove(int index) 
		{ 
            Tvinci.Data.TVMDataLoader.Protocols.GroupsMedia.File obj = FileCollection[index];
            FileCollection.Remove(obj);
			return obj;
        }

        public void Remove(object obj)
		{
            FileCollection.Remove(obj);
        }

		[XmlAttribute(AttributeName="PicUrl",Form=XmlSchemaForm.Unqualified)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __PicUrl;
		
		[XmlIgnore]
		public string PicUrl
		{ 
			get { return __PicUrl; }
			set { __PicUrl = value; }
		}

		[XmlAttribute(AttributeName="ID",Form=XmlSchemaForm.Unqualified,DataType="int")]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public int __ID;
		
		[XmlIgnore]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public bool __IDSpecified;
		
		[XmlIgnore]
		public int ID
		{ 
			get { return __ID; }
			set { __ID = value; __IDSpecified = true; }
		}

		[XmlAttribute(AttributeName="Title",Form=XmlSchemaForm.Unqualified,DataType="string")]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __Title;
		
		[XmlIgnore]
		public string Title
		{ 
			get { return __Title; }
			set { __Title = value; }
		}

		[XmlAttribute(AttributeName="Type",Form=XmlSchemaForm.Unqualified,DataType="string")]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __Type;
		
		[XmlIgnore]
		public string Type
		{ 
			get { return __Type; }
			set { __Type = value; }
		}

		[XmlAttribute(AttributeName="is_active",Form=XmlSchemaForm.Unqualified,DataType="int")]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public int __is_active;
		
		[XmlIgnore]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public bool __is_activeSpecified;
		
		[XmlIgnore]
		public int is_active
		{ 
			get { return __is_active; }
			set { __is_active = value; __is_activeSpecified = true; }
		}

		[XmlAttribute(AttributeName="TypeID",Form=XmlSchemaForm.Unqualified,DataType="int")]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public int __TypeID;
		
		[XmlIgnore]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public bool __TypeIDSpecified;
		
		[XmlIgnore]
		public int TypeID
		{ 
			get { return __TypeID; }
			set { __TypeID = value; __TypeIDSpecified = true; }
		}

		[XmlAttribute(AttributeName="FileCount",Form=XmlSchemaForm.Unqualified,DataType="int")]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public int __FileCount;
		
		[XmlIgnore]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public bool __FileCountSpecified;
		
		[XmlIgnore]
		public int FileCount
		{ 
			get { return __FileCount; }
			set { __FileCount = value; __FileCountSpecified = true; }
		}

		[XmlElement(Type=typeof(Tvinci.Data.TVMDataLoader.Protocols.GroupsMedia.File),ElementName="File",IsNullable=false,Form=XmlSchemaForm.Qualified)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public FileCollection __FileCollection;
		
		[XmlIgnore]
		public FileCollection FileCollection
		{
			get
			{
				if (__FileCollection == null) __FileCollection = new FileCollection();
				return __FileCollection;
			}
			set {__FileCollection = value;}
		}

		public Media()
		{
		}

		public void MakeSchemaCompliant()
		{
			if (FileCollection.Count == 0)
			{
				File _c = FileCollection.Add();
				_c.MakeSchemaCompliant();
			}
			else foreach (File _c in FileCollection) _c.MakeSchemaCompliant();
		}
	}


	[XmlRoot(ElementName="GroupMedias",IsNullable=false),Serializable]
	public partial class GroupMedias
	{

		[XmlElement(Type=typeof(root),ElementName="root",IsNullable=false,Form=XmlSchemaForm.Qualified)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public root __root;
		
		[XmlIgnore]
		public root root
		{
			get
			{
				if (__root == null) __root = new root();		
				return __root;
			}
			set {__root = value;}
		}

		[XmlElement(Type=typeof(response),ElementName="response",IsNullable=false,Form=XmlSchemaForm.Qualified)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public response __response;
		
		[XmlIgnore]
		public response response
		{
			get
			{
				if (__response == null) __response = new response();		
				return __response;
			}
			set {__response = value;}
		}

		public GroupMedias()
		{
		}

		public void MakeSchemaCompliant()
		{
			root.MakeSchemaCompliant();
			response.MakeSchemaCompliant();
		}
	}


	[XmlType(TypeName="root"),Serializable]
	public partial class root
	{

		[XmlElement(Type=typeof(request),ElementName="request",IsNullable=false,Form=XmlSchemaForm.Qualified)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public request __request;
		
		[XmlIgnore]
		public request request
		{
			get
			{
				if (__request == null) __request = new request();		
				return __request;
			}
			set {__request = value;}
		}

		public root()
		{
		}

		public void MakeSchemaCompliant()
		{
			request.MakeSchemaCompliant();
		}
	}


	[XmlType(TypeName="request"),Serializable]
	public partial class request
	{

		[XmlElement(Type=typeof(@params),ElementName="params",IsNullable=false,Form=XmlSchemaForm.Qualified)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public @params __params;
		
		[XmlIgnore]
		public @params @params
		{
			get
			{
				if (__params == null) __params = new @params();		
				return __params;
			}
			set {__params = value;}
		}

		public request()
		{
		}

		public void MakeSchemaCompliant()
		{
			@params.MakeSchemaCompliant();
		}
	}


	[XmlType(TypeName="params"),Serializable]
	public partial class @params
	{

		[XmlElement(ElementName="GroupID",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="int")]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public int __GroupID;
		
		[XmlIgnore]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public bool __GroupIDSpecified;
		
		[XmlIgnore]
		public int GroupID
		{ 
			get { return __GroupID; }
			set { __GroupID = value; __GroupIDSpecified = true; }
		}

		[XmlElement(ElementName="NumOfItems",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="int")]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public int __NumOfItems;
		
		[XmlIgnore]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public bool __NumOfItemsSpecified;
		
		[XmlIgnore]
		public int NumOfItems
		{ 
			get { return __NumOfItems; }
			set { __NumOfItems = value; __NumOfItemsSpecified = true; }
		}

		[XmlElement(ElementName="PageIndex",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="int")]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public int __PageIndex;
		
		[XmlIgnore]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public bool __PageIndexSpecified;
		
		[XmlIgnore]
		public int PageIndex
		{ 
			get { return __PageIndex; }
			set { __PageIndex = value; __PageIndexSpecified = true; }
		}

		[XmlElement(ElementName="PicSize",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="string")]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public string __PicSize;
		
		[XmlIgnore]
		public string PicSize
		{ 
			get { return __PicSize; }
			set { __PicSize = value; }
		}

		[XmlElement(ElementName="TypeID",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="int")]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public int __TypeID;
		
		[XmlIgnore]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public bool __TypeIDSpecified;
		
		[XmlIgnore]
		public int TypeID
		{ 
			get { return __TypeID; }
			set { __TypeID = value; __TypeIDSpecified = true; }
		}

		[XmlElement(ElementName="WithFiles",IsNullable=false,Form=XmlSchemaForm.Qualified,DataType="boolean")]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public bool __WithFiles;
		
		[XmlIgnore]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public bool __WithFilesSpecified;
		
		[XmlIgnore]
		public bool WithFiles
		{ 
			get { return __WithFiles; }
			set { __WithFiles = value; __WithFilesSpecified = true; }
		}

		public @params()
		{
			__GroupIDSpecified = true;
			__NumOfItemsSpecified = true;
			__PageIndexSpecified = true;
			PicSize = string.Empty;
			__TypeIDSpecified = true;
			__WithFilesSpecified = true;
		}

		public void MakeSchemaCompliant()
		{
		}
	}


	[XmlType(TypeName="response"),Serializable]
	public partial class response
	{
		[System.Runtime.InteropServices.DispIdAttribute(-4)]
		public IEnumerator GetEnumerator() 
		{
            return MediaCollection.GetEnumerator();
		}

		public Tvinci.Data.TVMDataLoader.Protocols.GroupsMedia.Media Add(Tvinci.Data.TVMDataLoader.Protocols.GroupsMedia.Media obj)
		{
			return MediaCollection.Add(obj);
		}

		[XmlIgnore]
		public Tvinci.Data.TVMDataLoader.Protocols.GroupsMedia.Media this[int index]
		{
			get { return (Tvinci.Data.TVMDataLoader.Protocols.GroupsMedia.Media) MediaCollection[index]; }
		}

		[XmlIgnore]
        public int Count 
		{
            get { return MediaCollection.Count; }
        }

        public void Clear()
		{
			MediaCollection.Clear();
        }

		public Tvinci.Data.TVMDataLoader.Protocols.GroupsMedia.Media Remove(int index) 
		{ 
            Tvinci.Data.TVMDataLoader.Protocols.GroupsMedia.Media obj = MediaCollection[index];
            MediaCollection.Remove(obj);
			return obj;
        }

        public void Remove(object obj)
		{
            MediaCollection.Remove(obj);
        }

		[XmlAttribute(AttributeName="total_num_of_items",Form=XmlSchemaForm.Unqualified,DataType="int")]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public int __total_num_of_items;
		
		[XmlIgnore]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public bool __total_num_of_itemsSpecified;
		
		[XmlIgnore]
		public int total_num_of_items
		{ 
			get { return __total_num_of_items; }
			set { __total_num_of_items = value; __total_num_of_itemsSpecified = true; }
		}

		[XmlElement(Type=typeof(Tvinci.Data.TVMDataLoader.Protocols.GroupsMedia.Media),ElementName="Media",IsNullable=false,Form=XmlSchemaForm.Qualified)]
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public MediaCollection __MediaCollection;
		
		[XmlIgnore]
		public MediaCollection MediaCollection
		{
			get
			{
				if (__MediaCollection == null) __MediaCollection = new MediaCollection();
				return __MediaCollection;
			}
			set {__MediaCollection = value;}
		}

		public response()
		{
		}

		public void MakeSchemaCompliant()
		{
			if (MediaCollection.Count == 0)
			{
				Media _c = MediaCollection.Add();
				_c.MakeSchemaCompliant();
			}
			else foreach (Media _c in MediaCollection) _c.MakeSchemaCompliant();
		}
	}
}
